# Library App
A simple ReactJS app to add and list books and authors to 

## Stack
  * SQLite
  * NodeJS
  * Express
  * ReactJS
<hr>

## Prequisites
  [NodeJS](https://nodejs.org/en/download/)
## Setup instructions
* Install packages `npm run setup`
* Development server `npm start`
    navigate to **/resetdb** to initialize and/or reset database
* Server only `npm run server`
* Client only `npm client`
* Build `npm run build`
* Run production server `npm run prod`